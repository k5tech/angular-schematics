# K5 Angular Schematics

> Custom angular schematics

[![NPM Version][npm-image]][npm-url]
[![Build Status][pipelines-image]][pipelines-url]
[![Coveralls Status][coveralls-image]][coveralls-url]

## Installation

```sh
npm install --save-dev @k5tech/angular-schematics
```

## Schematics

| Schematic | Example |
| --- | --- |
| [mat-palette](#material-palette) | `ng generate @k5tech/angular-schematics:mat-palette --primary '#00bcd4' --accent '#e91e63'`|

### Material Palette

The material palette (`mat-palette`) schematic generates an [Angular Material Design](https://github.com/angular/material2) compatible color palette.

`ng generate@k5tech/angular-schematics:mat-palette --primary '#HexColor' --accent '#HexColor'`

The generated palette is named `[theme].palette.scss` and placed in the specified `--path` parameter directory. Variable names are prefixed this the `--theme` parameter value.

#### Options

| Parameter | Default | Description |
| --- | --- | --- |
| `--primary` | #3f51b5 | Primary color hex code |
| `--accent` | #e91e63 | Accent color hex code |
| `--warn` | #f44336 | Warning color hex code |
| `--theme` | theme | Name of palette and scss file |
| `--path` | / | The path to create the palette |
| `--sourceDir` | src | The path of the source directory |

Default output `src/theme.palette.scss`

```scss
$theme-dark-primary-text: rgba(black, 0.87);
$theme-light-primary-text: white;

$theme-mat-primary: (
  50: #e8eaf6,
  100: #c5cbe9,
  200: #9fa8da,
  300: #7985cb,
  400: #5c6bc0,
  500: #3f51b5,
  600: #394aae,
  700: #3140a5,
  800: #29379d,
  900: #1b278d,
  A100: #c6cbff,
  A200: #939dff,
  A400: #606eff,
  A700: #4757ff,
  contrast: (
    50:  $theme-dark-primary-text,
    100: $theme-dark-primary-text,
    200: $theme-dark-primary-text,
    300: $theme-dark-primary-text,
    400: $theme-light-primary-text,
    500: $theme-light-primary-text,
    600: $theme-light-primary-text,
    700: $theme-light-primary-text,
    800: $theme-light-primary-text,
    900: $theme-light-primary-text,
    A100: $theme-dark-primary-text,
    A200: $theme-dark-primary-text,
    A400: $theme-light-primary-text,
    A700: $theme-light-primary-text,
  )
);

$theme-mat-accent: (
  50: #fce4ec,
  100: #f8bcd0,
  200: #f48fb1,
  300: #f06292,
  400: #ec407a,
  500: #e91e63,
  600: #e61a5b,
  700: #e31651,
  800: #df1247,
  900: #d90a35,
  A100: #ffffff,
  A200: #ffd0d8,
  A400: #ff9dae,
  A700: #ff8498,
  contrast: (
    50:  $theme-dark-primary-text,
    100: $theme-dark-primary-text,
    200: $theme-dark-primary-text,
    300: $theme-dark-primary-text,
    400: $theme-light-primary-text,
    500: $theme-light-primary-text,
    600: $theme-light-primary-text,
    700: $theme-light-primary-text,
    800: $theme-light-primary-text,
    900: $theme-light-primary-text,
    A100: $theme-dark-primary-text,
    A200: $theme-dark-primary-text,
    A400: $theme-dark-primary-text,
    A700: $theme-dark-primary-text,
  )
);

$theme-mat-warn: (
  50: #fee8e7,
  100: #fcc7c3,
  200: #faa19b,
  300: #f77b72,
  400: #f65f54,
  500: #f44336,
  600: #f33d30,
  700: #f13429,
  800: #ef2c22,
  900: #ec1e16,
  A100: #ffffff,
  A200: #ffe9e9,
  A400: #ffb8b6,
  A700: #ff9f9c,
  contrast: (
    50:  $theme-dark-primary-text,
    100: $theme-dark-primary-text,
    200: $theme-dark-primary-text,
    300: $theme-dark-primary-text,
    400: $theme-dark-primary-text,
    500: $theme-light-primary-text,
    600: $theme-light-primary-text,
    700: $theme-light-primary-text,
    800: $theme-light-primary-text,
    900: $theme-light-primary-text,
    A100: $theme-dark-primary-text,
    A200: $theme-dark-primary-text,
    A400: $theme-dark-primary-text,
    A700: $theme-dark-primary-text,
  )
);
```

> Tip: Using the angular cli `--force` parameter allows you to regenerate a palette file with new values.

#### Example

Regenerate to Nick's preferred.
![Primary](https://img.shields.io/badge/Primary-00bcd4-00bcd4.svg)
![Accent](https://img.shields.io/badge/Accent-e91e63-e91e63.svg)
![Warn](https://img.shields.io/badge/Warn-f44336-f44336.svg)

```sh
ng generate @k5tech/angular-schematics:mat-palette --primary '#00bcd4' --accent '#e91e63' --force
```

### Update HTML

Update selected content using the [cheerio](https://github.com/cheeriojs/cheerio) package.

#### Examples

```sh
ng generate @k5tech/angular-schematics:update-html --file src/index.html --selector="title" --text="My Custom App"
```

```sh
ng generate @k5tech/angular-schematics:update-html --file="tmp/test.html" --selector="meta[name=theme-color]" --attr='{"content": "#3f51b5"}'
```

[npm-image]: https://img.shields.io/npm/v/@k5tech/angular-schematics.svg
[npm-url]: https://npmjs.org/package/@k5tech/angular-schematics
[pipelines-image]:https://img.shields.io/bitbucket/pipelines/k5tech/angular-schematics.svg
[pipelines-url]:https://bitbucket.org/k5tech/angular-schematics/addon/pipelines/home
[coveralls-image]:https://img.shields.io/coveralls/bitbucket/k5tech/angular-schematics.svg
[coveralls-url]:https://coveralls.io/bitbucket/k5tech/angular-schematics