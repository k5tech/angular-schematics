import { normalize } from '@angular-devkit/core';
import * as cheerio from 'cheerio'
import {
  Rule,
  SchematicsException,
  // apply,
  // branchAndMerge,
  // chain,
  // mergeWith,
  // move,
  // template,
  // url,
  Tree,
  SchematicContext,
} from '@angular-devkit/schematics';

import { Schema as UpdateOptions } from './schema';

export default function (options: UpdateOptions): Rule {
  return (host: Tree, context: SchematicContext) => {
    context.logger.debug('Updating HTML file');

    const file = options.file ? normalize(options.file) : options.file;

    if (!file) {
      throw new SchematicsException(`file option is required.`);
    }

    const text = host.read(file);
    if (text === null) {
      throw new SchematicsException(`File ${file} does not exist.`);
    }
    const sourceText = text.toString('utf-8');

    const $ = cheerio.load(sourceText);
    // Select
    const selected = $(options.selector);

    // Run method depending on args
    if (options.text) {
      selected.text(unquote(options.text));
    }

    // Attributes
    if (options.addClass) {
      selected.addClass(options.addClass)
    }

    if (options.attr) {
      try {
        const attributes = JSON.parse(options.attr);
        for (const attribute in attributes) {
          selected.attr(attribute, attributes[attribute]);
        }
      } 
      catch (e) {
        throw new SchematicsException('Could not parse attr option json');
        // console.error(e);
      }
    }

    if (options.prop) {
      for (const prop in options.prop) {
        selected.prop(prop, options.prop[prop]);
      }
    }
    if (options.data) {
      for (const data in options.data) {
        selected.data(data, options.data[data]);
      }
    }
    if (options.val) {
      selected.val(options.val)
    }
    if (options.removeAttr) {
      selected.removeAttr(options.removeAttr)
    }
    if (options.addClass) {
      selected.addClass(options.addClass)
    }
    if (options.removeClass) {
      selected.removeClass(options.removeClass)
    }
    if (options.toggleClass) {
      selected.removeClass(options.toggleClass)
    }


    host.overwrite(file, $.html())

    return host;

  }

}

function unquote(string: string): string {
  let output = string;
  if (string.charAt(0) === '"' && string.charAt(string.length -1) === '"') {
      output = string.substr(1,string.length -2);
  }
  return output;
}