export interface Schema {
    file: string;
    selector: string;
    text?: string;
    // Attributes
    // attr?: { [attribute: string]: string };
    attr?: string;
    prop?: { [attribute: string]: string };
    data?: { [attribute: string]: string };
    val?: string;
    removeAttr?: string;
    addClass?: string;
    removeClass?: string;
    toggleClass?: string;
}