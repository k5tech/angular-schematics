import { VirtualTree } from '@angular-devkit/schematics';
import { SchematicTestRunner } from '@angular-devkit/schematics/testing';
import * as path from 'path';
import { Schema as UpdateOptions } from './schema';


describe('Update HTML Schematic (update-html)', () => {
  const schematicRunner = new SchematicTestRunner('schematics', path.join(__dirname, '../collection.json'));
  let appTree: VirtualTree;

  const defaultOptions: UpdateOptions = {
    file: '/src/index.html',
    selector: 'title',
  };

  beforeEach(() => {
    appTree = new VirtualTree();
    appTree.create(defaultOptions.file, defaultHTML());
  });


  describe('.attr', () => {

    it('updates attributes', () => {
      const options = { ...defaultOptions, selector: 'meta[name=theme-color]', attr: '{ "content": "#FFFFFF" }' };
      const tree = schematicRunner.runSchematic('update-html', options, appTree);
      const testFileContent = tree.readContent(options.file);
      expect(testFileContent).toMatch(/<meta name="theme-color" content="#FFFFFF">/);
    });

    it('throws error on bad formatted attr selector', () => {
      const options = { ...defaultOptions, selector: 'meta[name=theme-color]', attr: '{ content": "#FFFFFF" }' };
      expect(() => {schematicRunner.runSchematic('update-html', options, appTree)}).toThrowError();
    });

  });

  describe('manipulation', () => {
    it('can update text', () => {
      const options = { ...defaultOptions, selector: 'title', text: 'Not Default' };
      const tree = schematicRunner.runSchematic('update-html', options, appTree);
      const testFileContent = tree.readContent(options.file);
      expect(testFileContent).toMatch(/<title>Not Default<\/title>/);
    });

    it('can update quoted text', () => {
      const options = { ...defaultOptions, selector: 'title', text: '"Not Default"' };
      const tree = schematicRunner.runSchematic('update-html', options, appTree);
      const testFileContent = tree.readContent(options.file);
      expect(testFileContent).toMatch(/<title>Not Default<\/title>/);
    });
  });

  function defaultHTML(): string {
    return `
    <!doctype html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <title>Default</title>
      <base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="theme-color" content="#7f7f7f">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    </head>
    <body>
      <app-root></app-root>
    </body>
    </html>
    `
  }
});