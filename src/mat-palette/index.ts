import { normalize, strings } from '@angular-devkit/core';
import {
  Rule,
  SchematicsException,
  apply,
  branchAndMerge,
  chain,
  mergeWith,
  move,
  template,
  url,
} from '@angular-devkit/schematics';
import * as tinycolor from 'tinycolor2'

import { Schema as PaletteOptions } from './schema';

type HexColorString = string;

interface SwatchProperties {
  '50': tinycolorInstance,
  '100': tinycolorInstance,
  '200': tinycolorInstance,
  '300': tinycolorInstance,
  '400': tinycolorInstance,
  '500': tinycolorInstance,
  '600': tinycolorInstance,
  '700': tinycolorInstance,
  '800': tinycolorInstance,
  '900': tinycolorInstance,
  A100: tinycolorInstance,
  A200: tinycolorInstance,
  A400: tinycolorInstance,
  A700: tinycolorInstance,
  [key: string]: tinycolorInstance;
}

class Swatch {
  private colors: SwatchProperties

  constructor(color: HexColorString) {
    this.colors = this.generateFrom(color);
  }

  toObject() {
    let output: {
      [key: string]: string
    } = {};
    let contrast: {
      [key: string]: boolean
    } = {}
    for (let color in this.colors) {
      const colorObj = this.colors[color] as tinycolorInstance;
      output[color] = colorObj.toHexString()
      contrast[color] = colorObj.isLight();
    }
    return { ...output, contrast };
  }

  private generateFrom(hex: HexColorString) {
    // https://stackoverflow.com/questions/28503998/how-to-create-custom-palette-with-custom-color-for-material-design-app/36229022#36229022
    const baseLight = tinycolor('#ffffff');
    const baseDark = this.multiply(tinycolor(hex).toRgb(), tinycolor(hex).toRgb());
    const baseTriad = tinycolor(hex).tetrad();
    return {
      50: tinycolor.mix(baseLight, hex, 12),
      100: tinycolor.mix(baseLight, hex, 30),
      200: tinycolor.mix(baseLight, hex, 50),
      300: tinycolor.mix(baseLight, hex, 70),
      400: tinycolor.mix(baseLight, hex, 85),
      500: tinycolor.mix(baseLight, hex, 100),
      600: tinycolor.mix(baseDark, hex, 87),
      700: tinycolor.mix(baseDark, hex, 70),
      800: tinycolor.mix(baseDark, hex, 54),
      900: tinycolor.mix(baseDark, hex, 25),
      A100: tinycolor.mix(baseDark, baseTriad[4], 15).saturate(80).lighten(65),
      A200: tinycolor.mix(baseDark, baseTriad[4], 15).saturate(80).lighten(55),
      A400: tinycolor.mix(baseDark, baseTriad[4], 15).saturate(80).lighten(45),
      A700: tinycolor.mix(baseDark, baseTriad[4], 15).saturate(80).lighten(40)
    }
  }
  private multiply(rgb1: ColorFormats.RGB, rgb2: ColorFormats.RGB) {
    rgb1.b = Math.floor(rgb1.b * rgb2.b / 255);
    rgb1.g = Math.floor(rgb1.g * rgb2.g / 255);
    rgb1.r = Math.floor(rgb1.r * rgb2.r / 255);
    return tinycolor('rgb ' + rgb1.r + ' ' + rgb1.g + ' ' + rgb1.b);
  };
}

export class Palette extends Swatch {
  contrast: Swatch
  constructor(color: HexColorString) {
    super(color);
    this.contrast = new Swatch(color);
  }
}

export default function (options: PaletteOptions): Rule {
  options.path = options.path ? normalize(options.path) : options.path;
  const sourceDir = options.sourceDir;

  if (!sourceDir) {
    throw new SchematicsException(`sourceDir option is required.`);
  }

  const templateSource = apply(url('./files'), [
    template({
      ...strings,
      ...options,
      lightTextVariable: `${options.name}-light-primary-text`,
      darkTextVariable: `${options.name}-dark-primary-text`,
      primaryPalette: (new Palette(options.primary)).toObject(),
      accentPalette: (new Palette(options.accent)).toObject(),
      warnPalette: (new Palette(options.warn)).toObject(),
    }),
    move(sourceDir),
  ]);

  return chain([
    branchAndMerge(chain([
      mergeWith(templateSource),
    ])),
  ]);
}