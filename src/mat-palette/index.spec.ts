import { Tree } from '@angular-devkit/schematics';
import { SchematicTestRunner } from '@angular-devkit/schematics/testing';
const { parse } = require('scss-parser');
const createQueryWrapper = require('query-ast');
import * as path from 'path';

const collectionPath = path.join(__dirname, '../collection.json');

describe('Material Palette Schematic (mat-palette)', () => {

  it('creates a file', () => {
    const runner = new SchematicTestRunner('schematics', collectionPath);
    const tree = runner.runSchematic('mat-palette', {}, Tree.empty());
    expect(tree.files).toEqual(['/src/theme.palette.scss']);
  });

  it('allows the theme name to be specified', () => {
    const runner = new SchematicTestRunner('schematics', collectionPath);
    const examples = ['red', 'blue', 'green'];
    for (let name of examples) {
      const tree = runner.runSchematic('mat-palette', { name }, Tree.empty());
      expect(tree.files).toEqual([`/src/${name}.palette.scss`]);
    }
  });

  it('produces a color scss stylesheet', () => {
    const runner = new SchematicTestRunner('schematics', collectionPath);
    const tree = runner.runSchematic('mat-palette', {}, Tree.empty());
    const content = getFileContent(tree, '/src/theme.palette.scss');
    const ast = parse(content);
    expect(ast.type).toEqual('stylesheet');

    // const query = createQueryWrapper(ast);
    // const variables = query('variable').filter((n: any) => {
    //   // console.log(n);
    //   return !query(n).hasParent();
    // });
    // console.dir(variables);
  });

  describe('contents', () => {
    let variables: any;
    let query: any;
    let content: any;

    beforeAll(() => {
      const runner = new SchematicTestRunner('schematics', collectionPath);
      const tree = runner.runSchematic('mat-palette', {}, Tree.empty());
      content = getFileContent(tree, '/src/theme.palette.scss');
      const ast = parse(content);
      query = createQueryWrapper(ast);
      variables = query().find('variable');
    });
    
    it('defines a primary color', () => {
      expect((variables as any).filter((n: any) => query(n).value() === 'theme-mat-primary').length()).toEqual(1);
    });

    it('defines a accent color', () => {
      expect((variables as any).filter((n: any) => query(n).value() === 'theme-mat-accent').length()).toEqual(1);
    });

    it('defines a warning color', () => {
      expect((variables as any).filter((n: any) => query(n).value() === 'theme-mat-warn').length()).toEqual(1);
    });

    it('defines expected default colors', () => {
      // console.log(content)
      expect(content).toMatch(/500: #3f51b5,/); // Default primary 500 color
      expect(content).toMatch(/500: #e91e63,/); // Default accent 500 color
      expect(content).toMatch(/500: #f44336,/); // Default warning 500 color
    });

  });
});


function getFileContent(tree: Tree, path: string): string {
  const fileEntry = tree.get(path);

  if (!fileEntry) {
    throw new Error(`The file (${path}) does not exist.`);
  }

  return fileEntry.content.toString();
}