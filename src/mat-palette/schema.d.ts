export interface Schema {
    name: string;
    primary: string;
    accent: string;
    warn: string;

    path?: string;
    sourceDir?: string;
    /**
     * The root of the application.
     */
    appRoot?: string;

}